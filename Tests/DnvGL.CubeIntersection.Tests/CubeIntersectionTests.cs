﻿
using System;
using System.Collections.Generic;
using DnvGL.CubeIntersection;
using Xunit;

namespace DnvGL.CubeIntersection.Tests
{
    public class CubeIntersectionTests
    {
        [Theory]
        [MemberData(nameof(Data))]
        public void Check_Two_Object_Collide(Cube a, Cube b)
        {
            var isIntersected = Cube.IsIntersected(a, b);

            Assert.True(isIntersected);
        }

        public static IEnumerable<object[]> Data =>
            new List<object[]>
            {
                new object[] { new Cube(1, 2, 3, 2), new Cube(1, 2, 3, 2) },
                new object[] { new Cube(2, 2, 3, 2), new Cube(2, 2, 2, 5) },
                new object[] { new Cube(3, 2, 3, 3), new Cube(1, 5, 3, 2) }
            };
    }
}
