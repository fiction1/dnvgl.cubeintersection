﻿namespace DnvGL.CubeIntersection
{
    public class Cube
    {
        private readonly float _x;
        private readonly float _y;
        private readonly float _z;
        private readonly float _size;

        public Cube(float x, float y, float z, float size)
        {
            _x = x;
            _y = y;
            _z = z;
            _size = size;
        }

        public static bool IsIntersected(Cube a, Cube b)
        {
            bool overlapInXPlane = OverlapInPlane(a._x, b._x, a._size, b._size);
            bool overlapInYPlane = OverlapInPlane(a._y, b._y, a._size, b._size);
            bool overlapInZPlane = OverlapInPlane(a._z, b._z, a._size, b._size);

            return overlapInXPlane || overlapInYPlane || overlapInZPlane;
        }

        private static bool OverlapInPlane(float aAxis, float bAxis, float aSize, float bSize)
        {
            float aMinX = aAxis - aSize;
            float aMaxX = aAxis + aSize;

            float bMinX = bAxis - bSize;
            float bMaxX = bAxis + bSize;

            return aMaxX > bMinX || aMinX < bMaxX;
        }
    }
}